package com.gvozdev.codegen.filestests.output.var2.cpp.manual.withoop;

import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertTrue;

class FilesExistingTests {

    @Test
    void mainExists() {
        File file = new File("src/main/resources/files/output/var2/cpp/manual/withoop/main.cpp");
        assertTrue(file.exists());
    }

    @Test
    void mainExampleExists() {
        File file = new File("src/main/resources/files/output/var2/cpp/manual/withoop/mainExample.cpp");
        assertTrue(file.exists());
    }
}
