package com.gvozdev.codegen.filestests.input;

import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertTrue;

class FilesExistingTests {

    @Test
    void bogi6Exists() {
        File file = new File("src/main/resources/files/input/BOGI_1-6.dat");
        assertTrue(file.exists());
    }

    @Test
    void brdc0010Exists() {
        File file = new File("src/main/resources/files/input/brdc0010.18n");
        assertTrue(file.exists());
    }

    @Test
    void bshm10Exists() {
        File file = new File("src/main/resources/files/input/bshm_1-10.dat");
        assertTrue(file.exists());
    }

    @Test
    void hueg6Exists() {
        File file = new File("src/main/resources/files/input/HUEG_1-6.dat");
        assertTrue(file.exists());
    }

    @Test
    void hueg10Exists() {
        File file = new File("src/main/resources/files/input/hueg_1-10.dat");
        assertTrue(file.exists());
    }

    @Test
    void igrg0010Exists() {
        File file = new File("src/main/resources/files/input/igrg0010.18i");
        assertTrue(file.exists());
    }

    @Test
    void igsg0010Exists() {
        File file = new File("src/main/resources/files/input/igsg0010.18i");
        assertTrue(file.exists());
    }

    @Test
    void leij10Exists() {
        File file = new File("src/main/resources/files/input/leij_1-10.dat");
        assertTrue(file.exists());
    }

    @Test
    void onsa6Exists() {
        File file = new File("src/main/resources/files/input/ONSA_1-6.dat");
        assertTrue(file.exists());
    }

    @Test
    void onsa10Exists() {
        File file = new File("src/main/resources/files/input/onsa_1-10.dat");
        assertTrue(file.exists());
    }

    @Test
    void pots6Exists() {
        File file = new File("src/main/resources/files/input/POTS_6hours.dat");
        assertTrue(file.exists());
    }

    @Test
    void spt10Exists() {
        File file = new File("src/main/resources/files/input/spt0_1-10.dat");
        assertTrue(file.exists());
    }

    @Test
    void svtl10Exists() {
        File file = new File("src/main/resources/files/input/svtl_1-10.dat");
        assertTrue(file.exists());
    }

    @Test
    void tit10Exists() {
        File file = new File("src/main/resources/files/input/tit2_1-10.dat");
        assertTrue(file.exists());
    }

    @Test
    void titz6Exists() {
        File file = new File("src/main/resources/files/input/TITZ_6hours.dat");
        assertTrue(file.exists());
    }

    @Test
    void vis10Exists() {
        File file = new File("src/main/resources/files/input/vis0_1-10.dat");
        assertTrue(file.exists());
    }

    @Test
    void warn10Exists() {
        File file = new File("src/main/resources/files/input/warn_1-10.dat");
        assertTrue(file.exists());
    }

    @Test
    void warn6Exists() {
        File file = new File("src/main/resources/files/input/WARN_6hours.dat");
        assertTrue(file.exists());
    }

    @Test
    void zeck10Exists() {
        File file = new File("src/main/resources/files/input/zeck_1-10.dat");
        assertTrue(file.exists());
    }
}
