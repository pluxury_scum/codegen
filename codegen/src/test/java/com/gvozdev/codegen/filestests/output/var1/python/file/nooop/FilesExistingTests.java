package com.gvozdev.codegen.filestests.output.var1.python.file.nooop;

import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertTrue;

class FilesExistingTests {

    @Test
    void mainExists() {
        File file = new File("src/main/resources/files/output/var1/python/file/nooop/main.py");
        assertTrue(file.exists());
    }

    @Test
    void mainExampleExists() {
        File file = new File("src/main/resources/files/output/var1/python/file/nooop/mainExample.py");
        assertTrue(file.exists());
    }
}