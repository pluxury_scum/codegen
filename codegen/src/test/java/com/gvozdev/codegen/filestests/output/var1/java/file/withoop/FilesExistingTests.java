package com.gvozdev.codegen.filestests.output.var1.java.file.withoop;

import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertTrue;

class FilesExistingTests {

    @Test
    void mainExists() {
        File file = new File("src/main/resources/files/output/var1/java/file/withoop/Main.java");
        assertTrue(file.exists());
    }

    @Test
    void graphDrawerExists() {
        File file = new File("src/main/resources/files/output/var1/java/file/withoop/GraphDrawer.java");
        assertTrue(file.exists());
    }

    @Test
    void mainExampleExists() {
        File file = new File("src/main/resources/files/output/var1/java/file/withoop/MainExample.java");
        assertTrue(file.exists());
    }

    @Test
    void graphDrawerExampleExists() {
        File file = new File("src/main/resources/files/output/var1/java/file/withoop/GraphDrawerExample.java");
        assertTrue(file.exists());
    }
}