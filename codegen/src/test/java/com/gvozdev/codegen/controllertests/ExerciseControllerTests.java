package com.gvozdev.codegen.controllertests;

import com.gvozdev.codegen.exercise.ExerciseController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class ExerciseControllerTests {

    @Autowired
    ExerciseController controller;

    @Test
    void exerciseControllerLoads() {
        assertThat(controller).isNotNull();
    }
}
