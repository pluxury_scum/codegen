package com.gvozdev.codegen.weblayertests.var1;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
class PythonPageTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void fileWithOopPageStatusIsOk() throws Exception {
        mockMvc.perform(get("/api/var1?lang=python&file=true&oop=true"))
               .andDo(print())
               .andExpect(status().isOk());
    }

    @Test
    void fileNoOopPageStatusIsOk() throws Exception {
        mockMvc.perform(get("/api/var1?lang=python&file=true&oop=true"))
               .andDo(print())
               .andExpect(status().isOk());
    }

    @Test
    void manualWithOopPageStatusIsOk() throws Exception {
        mockMvc.perform(get("/api/var1?lang=python&file=false&oop=true"))
               .andDo(print())
               .andExpect(status().isOk());
    }

    @Test
    void manualNoOopPageStatusIsOk() throws Exception {
        mockMvc.perform(get("/api/var1?lang=python&file=false&oop=false"))
               .andDo(print())
               .andExpect(status().isOk());
    }
}
