package com.gvozdev.codegen.weblayertests.var2;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
class JavaPageTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void fileWithOopPageStatusIsOk() throws Exception {
        mockMvc.perform(get("/api/var2?lang=java&file=true&oop=true"))
               .andDo(print())
               .andExpect(status().isOk());
    }

    @Test
    void manualWithOopPageStatusIsOk() throws Exception {
        mockMvc.perform(get("/api/var2?lang=java&file=false&oop=true"))
               .andDo(print())
               .andExpect(status().isOk());
    }

    @Test
    void fileNoOopPageDoesNotExist() throws Exception {
        assertThrows(Exception.class, () -> {
            mockMvc.perform(get("/api/var2?lang=java&file=true&oop=false"));
        });
    }

    @Test
    void manualNoOopPageDoesNotExist() throws Exception {
        assertThrows(Exception.class, () -> {
            mockMvc.perform(get("/api/var2?lang=java&file=false&oop=false"));
        });
    }
}
