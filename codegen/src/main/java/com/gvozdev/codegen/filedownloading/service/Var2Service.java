package com.gvozdev.codegen.filedownloading.service;

import com.gvozdev.codegen.exercise.filereaders.Var2FileReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Var2Service {
    private final Var2FileReader var2FileReader;

    public Var2Service(Var2FileReader var2FileReader) {
        this.var2FileReader = var2FileReader;
    }

    public Map<String, String> getVar2Info() {
        List<String> satelliteNumbers = extractSatelliteNumbers();
        String satellite1Number = satelliteNumbers.get(0);
        String satellite2Number = satelliteNumbers.get(1);
        String satellite3Number = satelliteNumbers.get(2);

        List<String> allElevationAngles = extractElevationAngles(satelliteNumbers);
        String satellite1ElevationAngles = allElevationAngles.get(0);
        String satellite2ElevationAngles = allElevationAngles.get(1);
        String satellite3ElevationAngles = allElevationAngles.get(2);

        Map<String, String> info = new HashMap<>();
        info.put("satellite1Number", satellite1Number);
        info.put("satellite2Number", satellite2Number);
        info.put("satellite3Number", satellite3Number);

        info.put("satellite1ElevationAngles", satellite1ElevationAngles);
        info.put("satellite2ElevationAngles", satellite2ElevationAngles);
        info.put("satellite3ElevationAngles", satellite3ElevationAngles);
        return info;
    }

    private List<String> extractSatelliteNumbers() {
        List<String> satelliteNumbers = var2FileReader.getSatelliteNumbers();
        return satelliteNumbers;
    }

    private List<String> extractElevationAngles(List<String> satelliteNumbers) {
        List<String> allElevationAngles = new ArrayList<>();
        for (String satelliteNumber : satelliteNumbers) {
            String satelliteElevationAngles = "";
            char firstDigit = satelliteNumber.charAt(0);
            if (satelliteNumber.length() > 1) {
                char secondDigit = satelliteNumber.charAt(1);
                satelliteElevationAngles = var2FileReader.getElevationAngles(firstDigit, secondDigit, 2);
            } else {
                satelliteElevationAngles = var2FileReader.getElevationAngles(firstDigit, 1);
            }
            allElevationAngles.add(satelliteElevationAngles);
        }
        return allElevationAngles;
    }
}
