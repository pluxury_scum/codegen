package com.gvozdev.codegen.filedownloading.service;

import com.gvozdev.codegen.exercise.filereaders.Var3EphemerisFileReader;
import com.gvozdev.codegen.exercise.filereaders.Var3IonoFileReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Var3Service {
    private final Var3EphemerisFileReader ephemerisFileReader;
    private final Var3IonoFileReader forecastIonoFileReader;
    private final Var3IonoFileReader preciseIonoFileReader;
    private final String satelliteNumber;

    public Var3Service(Var3EphemerisFileReader ephemerisFileReader, Var3IonoFileReader forecastIonoFileReader,
        Var3IonoFileReader preciseIonoFileReader, String satelliteNumber) {
        this.ephemerisFileReader = ephemerisFileReader;
        this.forecastIonoFileReader = forecastIonoFileReader;
        this.preciseIonoFileReader = preciseIonoFileReader;
        this.satelliteNumber = satelliteNumber;
    }

    public Map<String, String> getVar3Info(String lat, String lon, String lat1, String lat2, String lon1, String lon2) {
        String gpsTime = getGpsTime();
        List<String> coefficients = extractIonCoefficients();
        String alpha = coefficients.get(0);
        String beta = coefficients.get(1);
        String forecastA1 = extractTec(lat2, lon2).get(0);
        String preciseA1 = extractTec(lat2, lon2).get(1);
        String forecastA2 = extractTec(lat1, lon2).get(0);
        String preciseA2 = extractTec(lat1, lon2).get(1);
        String forecastA3 = extractTec(lat1, lon1).get(0);
        String preciseA3 = extractTec(lat1, lon1).get(1);
        String forecastA4 = extractTec(lat2, lon1).get(0);
        String preciseA4 = extractTec(lat2, lon1).get(1);

        Map<String, String> info = new HashMap<>();
        info.put("lat", lat);
        info.put("lon", lon);
        info.put("lat1", lat1);
        info.put("lat2", lat2);
        info.put("lon1", lon1);
        info.put("lon2", lon2);
        info.put("gpsTime", gpsTime);
        info.put("alpha", alpha);
        info.put("beta", beta);
        info.put("forecastA1", forecastA1);
        info.put("forecastA2", forecastA2);
        info.put("forecastA3", forecastA3);
        info.put("forecastA4", forecastA4);
        info.put("preciseA1", preciseA1);
        info.put("preciseA2", preciseA2);
        info.put("preciseA3", preciseA3);
        info.put("preciseA4", preciseA4);
        return info;
    }

    private String getGpsTime() {
        double[] gpsTimeList = extractGpsTime();
        StringBuilder builder = new StringBuilder();
        for (double time : gpsTimeList) {
            builder.append(time).append(", ");
        }
        String gpsTime = builder.toString();
        return gpsTime;
    }

    private double[] extractGpsTime() {
        double[] gpsTimeList;
        char firstDigit = satelliteNumber.charAt(0);
        if (satelliteNumber.length() > 1) {
            char secondDigit = satelliteNumber.charAt(1);
            gpsTimeList = ephemerisFileReader.getGpsTime(firstDigit, secondDigit, 2);
        } else {
            gpsTimeList = ephemerisFileReader.getGpsTime(firstDigit, 1);
        }
        return gpsTimeList;
    }

    private List<String> extractIonCoefficients() {
        List<String> ionCoefficients = new ArrayList<>();
        String alpha = ephemerisFileReader.getAlpha();
        String beta = ephemerisFileReader.getBeta();
        ionCoefficients.add(alpha);
        ionCoefficients.add(beta);
        return ionCoefficients;
    }

    private List<String> extractTec(String lat, String lon) {
        char firstDigit = lat.charAt(0);
        char secondDigit = lat.charAt(1);
        char thirdDigit = lat.charAt(2);
        List<String> tecLists = new ArrayList<>();
        String forecastTecList = forecastIonoFileReader.getTec(firstDigit, secondDigit, thirdDigit, lon, 304);
        String preciseTecList = preciseIonoFileReader.getTec(firstDigit, secondDigit, thirdDigit, lon, 394);
        tecLists.add(forecastTecList);
        tecLists.add(preciseTecList);
        return tecLists;
    }
}
