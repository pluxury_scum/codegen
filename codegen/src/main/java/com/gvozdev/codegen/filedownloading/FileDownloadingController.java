package com.gvozdev.codegen.filedownloading;

import com.gvozdev.codegen.exercise.filereaders.Var1FileReader;
import com.gvozdev.codegen.exercise.filereaders.Var2FileReader;
import com.gvozdev.codegen.exercise.filereaders.Var3EphemerisFileReader;
import com.gvozdev.codegen.exercise.filereaders.Var3IonoFileReader;
import com.gvozdev.codegen.filedownloading.service.Var1Service;
import com.gvozdev.codegen.filedownloading.service.Var2Service;
import com.gvozdev.codegen.filedownloading.service.Var3Service;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

@Controller
@RequestMapping("/download")
public class FileDownloadingController {

    @GetMapping("/{var}/{lang}/{ifFile}/{ifOop}/{file}")
    public void downloadFile(HttpServletResponse response,
                             @PathVariable(value = "var", required = true) String var,
                             @PathVariable(value = "lang", required = true) String lang,
                             @PathVariable(value = "ifFile", required = true) String ifFile,
                             @PathVariable(value = "ifOop", required = true) String ifOop,
                             @PathVariable(value = "file", required = true) String fileName) {
        String codeDirectory = "src/main/resources/files/output/"
                               .concat(var).concat("/")
                               .concat(lang).concat("/")
                               .concat(ifFile).concat("/")
                               .concat(ifOop).concat("/")
                               .concat(fileName);
        Path codeFile = Paths.get(codeDirectory);
        response.setContentType("application/pdf");
        response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
        try {
            Files.copy(codeFile, response.getOutputStream());
            response.getOutputStream().flush();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @GetMapping("/resources")
    public void downloadResources(HttpServletResponse response) {
        String fileName = "resources.rar";
        String resourcesDirectory = "src/main/resources/files/output/" + fileName;
        Path resourcesFile = Paths.get(resourcesDirectory);
        response.setContentType("application/rar");
        response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
        try {
            Files.copy(resourcesFile, response.getOutputStream());
            response.getOutputStream().flush();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @GetMapping("/extra/{var}")
    public String getExtra(@PathVariable(value = "var", required = true) String varNumber) {
        String path = "forms/var" + varNumber + "/extra";
        return path;
    }

    @PostMapping("/extra/print/1")
    public String printExtraInfoVar1(@RequestParam(value = "filename", required = true) String fileName,
                                     Model model) throws IOException {
        Var1FileReader fileReader = new Var1FileReader(fileName);
        Var1Service service = new Var1Service(fileReader);
        Map<String, String> info = service.getVar1Info();
        model.mergeAttributes(info);
        return "extrainfo/var1/show";
    }

    @PostMapping("/extra/print/2")
    public String printExtraInfoVar2(@RequestParam(value = "filename", required = true) String fileName,
                                     Model model) throws IOException {
        Var2FileReader var2FileReader = new Var2FileReader(fileName);
        Var2Service service = new Var2Service(var2FileReader);
        Map<String, String> info = service.getVar2Info();
        model.mergeAttributes(info);
        return "extrainfo/var2/show";
    }

    @PostMapping("/extra/print/3")
    public String printExtraInfoVar3(@RequestParam(value = "lat", required = true) String lat,
                                     @RequestParam(value = "lon", required = true) String lon,
                                     @RequestParam(value = "lat1", required = true) String lat1,
                                     @RequestParam(value = "lat2", required = true) String lat2,
                                     @RequestParam(value = "lon1", required = true) String lon1,
                                     @RequestParam(value = "lon2", required = true) String lon2,
                                     @RequestParam(value = "sat", required = true) String sat,
                                     Model model) throws IOException {
        Var3EphemerisFileReader ephemerisFileReader = new Var3EphemerisFileReader();
        Var3IonoFileReader ionoFileReaderForecast = new Var3IonoFileReader("igrg0010.18i");
        Var3IonoFileReader ionoFileReaderPrecise = new Var3IonoFileReader("igsg0010.18i");
        Var3Service service = new Var3Service(ephemerisFileReader, ionoFileReaderForecast, ionoFileReaderPrecise, sat);
        Map<String, String> info = service.getVar3Info(lat, lon, lat1, lat2, lon1, lon2);
        model.mergeAttributes(info);
        return "extrainfo/var3/show";
    }
}
