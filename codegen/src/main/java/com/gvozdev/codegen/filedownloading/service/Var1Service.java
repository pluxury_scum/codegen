package com.gvozdev.codegen.filedownloading.service;

import com.gvozdev.codegen.exercise.filereaders.Var1FileReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Var1Service {
    private final Var1FileReader var1FileReader;

    public Var1Service(Var1FileReader var1FileReader) {
        this.var1FileReader = var1FileReader;
    }

    public Map<String, String> getVar1Info() {
        List<String> satelliteNumbers = extractSatelliteNumbers();
        String satellite1Number = satelliteNumbers.get(0);
        String satellite2Number = satelliteNumbers.get(1);
        String satellite3Number = satelliteNumbers.get(2);

        List<List<String>> allPseudoRanges = extractPseudoRanges(satelliteNumbers);
        String satellite1P1 = allPseudoRanges.get(0).get(0);
        String satellite1P2 = allPseudoRanges.get(0).get(1);
        String satellite2P1 = allPseudoRanges.get(1).get(0);
        String satellite2P2 = allPseudoRanges.get(1).get(1);
        String satellite3P1 = allPseudoRanges.get(2).get(0);
        String satellite3P2 = allPseudoRanges.get(2).get(1);

        Map<String, String> info = new HashMap<>();
        info.put("satellite1Number", satellite1Number);
        info.put("satellite2Number", satellite2Number);
        info.put("satellite3Number", satellite3Number);

        info.put("satellite1P1", satellite1P1);
        info.put("satellite1P2", satellite1P2);
        info.put("satellite2P1", satellite2P1);
        info.put("satellite2P2", satellite2P2);
        info.put("satellite3P1", satellite3P1);
        info.put("satellite3P2", satellite3P2);
        return info;
    }

    private List<String> extractSatelliteNumbers() {
        List<String> satelliteNumbers = var1FileReader.getSatelliteNumbers();
        return satelliteNumbers;
    }

    private List<List<String>> extractPseudoRanges(List<String> satelliteNumbers) {
        List<List<String>> allPseudoRanges = new ArrayList<>();
        for (String satelliteNumber : satelliteNumbers) {
            List<String> satellitePseudoRanges;
            char firstDigit = satelliteNumber.charAt(0);
            if (satelliteNumber.length() > 1) {
                char secondDigit = satelliteNumber.charAt(1);
                satellitePseudoRanges = var1FileReader.getPseudoRanges(firstDigit, secondDigit, 2);
            } else {
                satellitePseudoRanges = var1FileReader.getPseudoRanges(firstDigit, 1);
            }
            allPseudoRanges.add(satellitePseudoRanges);
        }
        return allPseudoRanges;
    }
}
