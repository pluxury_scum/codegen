package com.gvozdev.codegen.exercise;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("api")
public class ExerciseController {

    @GetMapping("/{varNumber}")
    public String getCode(@PathVariable String varNumber,
                          @RequestParam(value = "lang", required = true) String language,
                          @RequestParam(value = "file", required = true) boolean ifFile,
                          @RequestParam(value = "oop", required = true) boolean ifOop) {
        ExercisePage factory = new ExercisePage(varNumber, language, ifFile, ifOop);
        String exercisePage = factory.getExercisePage();
        return exercisePage;
    }
}
